call plug#begin('~/.vim/plugged')

Plug 'ctrlpvim/ctrlp.vim'
Plug 'w0rp/ale'
Plug 'elmcast/elm-vim'

call plug#end()
